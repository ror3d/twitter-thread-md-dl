import jmespath
import json

#%%
with open("twitter.com.har", "r", encoding="utf8") as f:
    j = json.load(f)

#%%

tweets = jmespath.search("log.entries[?contains(request.url, 'TweetDetail')].response.content.size", j)


#%%

tweets = [json.loads(t) for t in tweets]

#%%

ids = jmespath.search("[*].data.threaded_conversation_with_injections_v2.instructions[?type=='TimelineAddEntries'].entries[][?contains(entryId, 'conversationthread-')].entryId[]", tweets)

#%%

ids = [i.replace('conversationthread-','') for i in ids]

#%%

with open("replies.csv", "w") as f:
    for i in ids:
        f.write("{}\n".format(i))
