# Twitter Thread Markdown Downloader

## Setup

You need a twitter application with api access for this to work https://developer.twitter.com

Create a `config.ini` file like

```
[secrets]
app-id=20000000
consumer-key=XXXXXXXXXXXXXX
consumer-secret=XXXXXXXXXXXXXXXX
bearer=XXXXXXXXXXXXXXXXXX
access-token=XXXXXXXXXXXXXXXX
access-secret=XXXXXXXXXXXXXX
client-id=XXXXXXXXXXXXXXX
client-secret=XXXXXXXXXXXX
```

with your application information for each field.

## Usage

Run like

    $ python dl-thread.py --id <tweet-id>

to download the thread ending at the given id (it goes from last to first, twitter doesn't give a good way to go from first to last in the API)

To obtain your bookmarks, run as

    $ python dl-thread.py --bookmarks

This will give you an oauth2 url, go to it, grant access, and copy the url that sends you to into the application. It will then save your bookmarks into a `bookmarks.csv` file.

To download all the thread in a list stored in a file, run like

    $ python dl-thread.py --list bookmarks.csv

This will run like with `--id` but for each line in the given file


### All replies to a tweet

This is a lot more manual:

1. Start the developer tools of the browser (I used chrome, other browsers might work differently here) and open the `Network` tab
2. Reload the page, and scroll through to view all the replies
3. Export HAR file
4. Load the HAR file following the `har-parsin.py` script (I don't know if the script will work by itself, it just has the commands I used in the CLI)
5. The `replies.csv` should now have the IDs of all the replies to the tweet we looked at.
6. Call dl-thread:
```
$ python dl-thread.py --replies replies.csv
```

This will create a single file with all the replies in the csv file. If you want the original tweet, add the id manually at the beginning of the csv file
