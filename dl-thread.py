import tweepy
import configparser
import re
import urllib
import shutil
import os
import argparse


#%%

config = configparser.ConfigParser(interpolation=None)
config.read("config.ini")

auth = tweepy.OAuth1UserHandler(
    consumer_key=config["secrets"]["consumer-key"],
    consumer_secret=config["secrets"]["consumer-secret"],
    access_token=config["secrets"]["access-token"],
    access_token_secret=config["secrets"]["access-secret"]
)
api = tweepy.API(auth, wait_on_rate_limit=True)

def get_tweet_and_parent(id):
    try:
        tw = api.get_status(id, tweet_mode='extended')
        return tw, tw.in_reply_to_status_id
    except tweepy.errors.HTTPException as e:
        text = ""
        for ae in e.api_errors:
            text += "\n*{}*".format(ae["message"])
        print("WARNING: Thread truncated. Last good tweet was https://twitter.com/{}/status/{}".format(tw_list[-1].author.screen_name, tw_list[-1].id))
        return text, None
    except Exception as e:
        text = ""
        text += "\n*{}*".format(str(e))
        print("WARNING: Thread truncated. Last good tweet was https://twitter.com/{}/status/{}".format(tw_list[-1].author.screen_name, tw_list[-1].id))
        return text, None

def collect_thread(id):
    global api

    print(f"Collecting thread for id {id}...")
    tw_list = []
    while id:
        tw, id = get_tweet_and_parent(id)
        tw_list.append(tw)

    print(f"{len(tw_list)} Tweets found in thread")

    tw_list.reverse()
    return tw_list


def get_bookmarks():
    #client = tweepy.Client(
    #    consumer_key=config["secrets"]["consumer-key"],
    #    consumer_secret=config["secrets"]["consumer-secret"],
    #    access_token=config["secrets"]["access-token"],
    #    access_token_secret=config["secrets"]["access-secret"]
    #)

    # OAUTH 2.0

    oauth2_user_handler = tweepy.OAuth2UserHandler(
        client_id=config["secrets"]["client-id"],
        redirect_uri="https://ror3d.xyz",
        scope=["tweet.read", "like.read", "bookmark.read", "users.read"],
        # Client Secret is only necessary if using a confidential client
        client_secret=config["secrets"]["client-secret"]
    )

    print(oauth2_user_handler.get_authorization_url())

    auth2_url = input("Response URL: ")

    auth2_token = oauth2_user_handler.fetch_token(auth2_url)
    print("Auth token:", auth2_token)

    client = tweepy.Client(auth2_token['access_token'],
        consumer_key=config["secrets"]["consumer-key"],
        consumer_secret=config["secrets"]["consumer-secret"],
        access_token=config["secrets"]["access-token"],
        access_token_secret=config["secrets"]["access-secret"],
        wait_on_rate_limit=True
    )
    return client.get_bookmarks().data

#%%

def local_url(url, media_dir="media/"):
    url = url[url.rfind("/")+1:]
    return media_dir + re.sub(r"\?.+$", "", url)

def download_media(m, media_dir):
    file_path = local_url(m, media_dir)
    if not os.path.exists(file_path):
        print(f"Downloading {m}...")
        with urllib.request.urlopen(m) as remote, open(file_path, 'wb') as local:
            shutil.copyfileobj(remote, local)

def extract_text(tw, media_dir="media/"):
    md = ""
    profile_pic_url = tw.author.profile_image_url_https
    download_media(profile_pic_url, media_dir)
    md += "#### ![]({2}) [{0} (@{1})](https://twitter.com/{1})\n".format(tw.author.name, tw.author.screen_name, local_url(profile_pic_url, media_dir))
    md += "\n"
    text = tw.full_text

    # Expand urls
    urls = [u for u in tw.entities["urls"]]
    if len(urls) > 0:
        urls.sort(key=lambda u: u["indices"][0])
        text_expanded = ""
        prev_expand_end = 0
        for url in urls:
            text_expanded += text[prev_expand_end:url["indices"][0]]
            text_expanded += url["expanded_url"]
            prev_expand_end = url["indices"][1]
        text_expanded += text[prev_expand_end:]
        text = text_expanded

    # Clean up ending url
    text = re.sub(r" https://t.co/[a-zA-Z0-9]+$", "", text)

    if tw.is_quote_status:
        global api
        try:
            qt = api.get_status(tw.quoted_status_id, tweet_mode='extended')
            text += "\n" + extract_text(qt, media_dir)
        except tweepy.errors.HTTPException as e:
            for ae in e.api_errors:
                text += "\n> *{}*".format(ae["message"])
        except Exception as e:
            text += "\n> *{}*".format(str(e))

    # Add permalink
    md += "[{}](https://twitter.com/{}/status/{})\n".format(tw.created_at.strftime("%Y-%m-%d %H:%M"), tw.author.screen_name, tw.id)

    # Add text as quote
    md += "> " + text.replace("\n", "\n> ")

    md += "\n>"
    md += "\n"

    # Add media
    if "media" in tw.entities:
        for media in tw.extended_entities["media"]:
            if media["type"] == "photo":
                url = media["media_url_https"]
                if url.find("?") >= 0:
                    url += "&"
                else:
                    url += "?"
                url += "name=4096x4096"
                download_media(url, media_dir)

            elif media["type"] == "video" or media["type"] == "animated_gif":
                urls = [v for v in media["video_info"]["variants"] if v["content_type"] == "video/mp4"]
                urls.sort(key=lambda x: x["bitrate"])
                url = urls[-1]["url"]
                download_media(url, media_dir)

            else:
                print("Unknown media type {} for {}".format(media["type"], media["url"]))
                url = media["url"]
            md += "> ![]({})\n".format(local_url(url, media_dir))
        md += "\n"

    return md

def tweet_list_to_markdown(ls):
    md = ""

    first = 0
    while type(ls[first]) == str:
        md += ls[first]
        md += "---\n"
        md += "\n"
        first += 1

    user = ls[first].author.screen_name
    twid = ls[-1].id
    twid = f"{user}-{twid}"

    os.makedirs(f"media/{twid}/", exist_ok=True)

    for t in ls[first:]:
        md += extract_text(t, f"media/{twid}/")
        md += "---\n"
        md += "\n"

    with open(f"{twid}.md", "w", encoding="utf8") as f:
        f.write(md)

#%%

arg_parser = argparse.ArgumentParser(
                prog="Twitter Thread Downloader",
                description="Downloads threads. Needs to receive the last tweet in a thread to properly download it entirely")
arg_parser.add_argument("--id", help="Define a tweet id to download from")
arg_parser.add_argument("--bookmarks", help="Download threads from your bookmarks", action="store_true")
arg_parser.add_argument("--list", help="File with list of ids to download")
arg_parser.add_argument("--replies", help="File with list of ids to download into a single file")

args = arg_parser.parse_args()

if args.id:
    tweet_list_to_markdown(collect_thread(args.id))
elif args.bookmarks:
    print("Reading bookmarks")
    bookmarks = get_bookmarks()
    print("{} bookmarks found".format(len(bookmarks)))
    with open("bookmarks.csv", "w") as f:
        for b in bookmarks:
            f.write("{}\n".format(b.id))
elif args.list:
    ids = []
    with open(args.list, "r") as f:
        ids = f.readlines()
    print("{} ids to read".format(len(ids)))
    for i, b in enumerate(ids):
        print(f"Tweet {i} of {len(ids)}")
        tweet_list_to_markdown(collect_thread(b.strip()))
elif args.replies:
    ids = []
    with open(args.replies, "r") as f:
        ids = f.readlines()
    print("{} ids to read".format(len(ids)))
    twls = []
    for i in ids:
        tw, _ = get_tweet_and_parent(i.strip())
        twls.append(tw)
    tweet_list_to_markdown(twls)
else:
    arg_parser.print_help()

